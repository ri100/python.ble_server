#!/usr/bin/env bash

sudo apt install python3-pip

pip3 install -r ../requirements.txt

sudo cp systemd/ble_server.service /etc/systemd/system/ble_server.service
sudo chown root:root /etc/systemd/system/ble_server.service
sudo chmod 664 /etc/systemd/system/ble_server.service

sudo systemctl daemon-reload

sudo systemctl enable ble_server.service

sudo systemctl start ble_server.service