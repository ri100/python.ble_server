from domain.ble_device import BleDevice
from service.logger import LoggerWrapper


class Printer:

    def __init__(self, mac_list: list = None):
        self.mac_list = mac_list
        self.logger = LoggerWrapper()

    def _notify(self, ble_device, state):
        if self.mac_list is None:
            self.logger.info("%s \033[1m%s\033[0m %s last seen at %s %f sec ago" % (
                state, ble_device.name, ble_device.mac, ble_device.last_seen_date(), ble_device.last_seen_sec()))
        elif ble_device.mac in self.mac_list:
            self.logger.info("%s \033[1m%s\033[0m %s last seen at %s %f sec ago" % (
                state, ble_device.name, ble_device.mac, ble_device.last_seen_date(), ble_device.last_seen_sec()))

    def __ble_present__(self, ble_device: BleDevice):
        self._notify(ble_device, "\033[0;32m[IN]\033[0m")

    def __ble_not_present__(self, ble_device: BleDevice):
        self._notify(ble_device, "\033[0;31m[OUT]\033[0m")
