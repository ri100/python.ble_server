import re
import socket
import threading
from time import time
from domain.ble_device import BleDevice
from domain.one_of import OneOf
from service.logger import LoggerWrapper

ble_devices = dict()
location = dict()
lock = threading.Lock()


class BleServer(threading.Thread):

    def __init__(self, devices_timeouts: dict, server_timeout=20, bind=('', 1234), grouping: dict = None):
        threading.Thread.__init__(self)
        self.server_timeout = server_timeout
        self.grouping = grouping  # type: dict
        self.shutdown_flag = threading.Event()
        self.bind = bind
        self.devices_timeouts = devices_timeouts
        self.logger = LoggerWrapper()

    def _get_ble(self, name: str, mac: str, rssi: int) -> BleDevice:
        if mac in self.devices_timeouts:
            expire_in = self.devices_timeouts[mac]
        else:
            expire_in = 30
        return BleDevice(name, mac, expire_in=expire_in, rssi=rssi)

    def run(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.settimeout(self.server_timeout)
        sock.bind(self.bind)
        self.logger.info("[START] Server waiting for messages: ")
        while not self.shutdown_flag.is_set():
            try:
                data, addr = sock.recvfrom(1024)
                command = data.decode("utf-8")
                matched = re.match(r'^\[([A-Z]{3})\]', command, re.I)
                if matched:
                    cmd = matched.group(1)
                    if cmd == 'SET':
                        # SET mac rssi device_name timestamp scanner_name
                        matched = re.match(
                            r'^\[([A-Z]{3})\] ([0-9a-f]{2}(?::[0-9a-f]{2}){5}) ([0-9\-]+) ([a-zA-Z0-9\-]+) ([0-9]+) ([a-zA-Z0-9\-]+)',
                            command, re.I)
                        if matched:

                            mac = matched.group(2)  # type: str
                            rssi = int(matched.group(3))  # type: int
                            name = matched.group(4)  # type: str
                            timestamp = int(matched.group(5))  # type: int
                            scanner_name = matched.group(6)  # type: str

                            defined_ble_devices = []
                            if self.grouping is not None:
                                for group_mac, grouped_mac_list in self.grouping.items():  # type: str, OneOf
                                    if grouped_mac_list.has_mac(mac):
                                        defined_ble_devices.append(group_mac)

                            for mac in defined_ble_devices:

                                with lock:

                                    if mac in ble_devices:
                                        discovered_ble = ble_devices[mac]
                                    else:
                                        discovered_ble = self._get_ble(name, mac, rssi)

                                    update_ok = discovered_ble.update(timestamp, rssi, scanner_name=scanner_name)

                                    if update_ok:
                                        if mac in ble_devices:
                                            self.logger.info(
                                                "\033[0;33m[SET]\033[0m \033[1m%s\033[0m %s:%s from \033[1m%s\033[0m %s:%s" % (
                                                    name,
                                                    mac,
                                                    str(rssi),
                                                    scanner_name,
                                                    timestamp,
                                                    str(float(timestamp) - time())))

                                        else:
                                            self.logger.info(
                                                "\033[0;32m[NEW]\033[0m \033[1m%s\033[0m %s:%s -> exp:%s, t:%s: d:%s" % (
                                                    name, mac, addr, discovered_ble.expire_in, timestamp, str(
                                                        float(timestamp) - time())))

                                    else:
                                        if mac in ble_devices:
                                            msg = str.encode(
                                                '\033[0;31m[ERR]\033[0m scanner %s: Could not update timestamp %s device registered with %s' % (
                                                    scanner_name, timestamp, ble_devices[mac].time))
                                        else:
                                            msg = str.encode(
                                                '\033[0;31m[ERR]\033[0m scanner %s: Could not update timestamp %s now is %s' % (
                                                    scanner_name, timestamp, int(time())))
                                        sock.sendto(msg, addr)
                                        continue

                                    ble_devices[mac] = discovered_ble

                                    self._save_location(mac, name, rssi, scanner_name, timestamp)

                                sock.sendto(str.encode('\033[0;32m[OK]\033[0m Recognized as %s in %s ' % (mac, scanner_name)),
                                            addr)
                        else:
                            sock.sendto(str.encode('\033[0;31m[ERR]\033[0m Could not matched %s' % command), addr)

                    elif cmd == 'GET':
                        matched = re.match(
                            r'^\[([A-Z]{3})\] ([0-9a-f]{2}(?::[0-9a-f]{2}){5}|[A-Za-z0-9]+)', command,
                            re.I)
                        if matched:
                            mac = matched.group(2)
                            self.logger.info("\033[0;34m[GET]\033[0m %s" % mac)
                            with lock:
                                if mac in ble_devices:
                                    sock.sendto(str.encode("[GET] %s %s %s %s %s %s" %
                                                           (ble_devices[mac].mac,
                                                            ble_devices[mac].rssi,
                                                            ble_devices[mac].name,
                                                            int(ble_devices[mac].time),
                                                            ble_devices[mac].scanner_name,
                                                            ble_devices[mac].is_gone())),
                                                addr)
                        else:
                            sock.sendto(str.encode("[FAIL]"), addr)

                else:
                    self.logger.info('[TRASH] ' + command.rstrip())
                    sock.sendto(str.encode('[FAIL]'), addr)
            except socket.timeout:
                self.logger.error("\033[0;31m[ERR]\033[0m Timeout - no data received in %s" % self.server_timeout)
            except Exception as e:
                self.logger.error("\033[0;31m[ERR]\033[0m " + str(e))

        self.logger.info('[END] Server ends')

    def _save_location(self, mac: str, name: str, rssi: int, scanner_name: str, timestamp: int):
        if mac in location and scanner_name in location[mac]:
            location_ble = location[mac][scanner_name]
            location_ble.update(timestamp, rssi, scanner_name=scanner_name)
        else:
            location_ble = self._get_ble(name, mac, rssi)
            location_ble.update(timestamp, rssi, scanner_name=scanner_name)
            if mac not in location:
                location[mac] = {scanner_name: location_ble}
            else:
                location[mac].update({scanner_name: location_ble})
