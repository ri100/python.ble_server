import os
import pwd
import grp
import requests
from domain.ble_device import BleDevice
from domain.location_report import LocationReport
from service.logger import LoggerWrapper
from service.server import ble_devices, location, lock
from time import sleep


class NotificationManager:

    def __init__(self, on_discovery_change: dict = None, on_location_change: dict = None, report_location: list = None):
        self.report_location = report_location
        self.on_location = on_location_change
        self.on_discovery = on_discovery_change
        self.logger = LoggerWrapper()

    @staticmethod
    def _should_be_deleted(ble_device):
        return ble_device.is_gone() and ble_device.last_seen_sec() > 60 * 60 * 24  # 1 day

    @staticmethod
    def _touch(path, user, group):
        with open(path, 'a'):
            os.utime(path, None)
        uid = pwd.getpwnam(user).pw_uid
        gid = grp.getgrnam(group).gr_gid
        os.chown(path, uid, gid)

    def notify(self):
        while True:
            self.report_location_change()

            sleep(2)

            self.report_discovery_change()

            sleep(2)

            self.report_location_status()

    def report_location_status(self):
        with lock:
            if self.report_location is not None:
                for loc in self.report_location:  # type: LocationReport
                    if loc.every.passed():
                        body = loc.prepare_data(location)
                        try:
                            if len(body) > 0:
                                requests.post(url=loc.send_to, json=body)
                                self.logger.info('\033[1m[INF]\033[0m LocationReport: %s' % str(body))
                        except Exception as e:
                            self.logger.error('\033[31;1m[ERR]\033[0m %s' % str(e))

    def report_discovery_change(self):
        with lock:
            for ble_mac, ble_device in list(ble_devices.items()):
                if type(ble_device) is BleDevice:

                    filename = '/tmp/' + ble_mac + '.ble'

                    # remove old
                    if self._should_be_deleted(ble_device):
                        del ble_devices[ble_mac]
                        if os.path.exists(filename):
                            os.remove(filename)
                        self.logger.info(
                            "\033[0;31m(DEL)\033[0m \033[1m%s\033[0m %s removed from memory" % (
                                ble_device.name, ble_device.mac))

                    if self.on_discovery is not None:
                        # is device configured to send notifications?
                        if ble_mac in self.on_discovery:
                            gone = ble_device.is_gone()
                            if ble_device.is_state_changed(gone):

                                ble_device.set_last_state(gone=gone)

                                if gone:
                                    self.logger.info(
                                        "\033[37;41m(GON)\033[0m \033[1m%s\033[0m %s:%s triggered OFF" % (
                                            ble_device.name,
                                            ble_device.mac,
                                            ble_device.rssi)
                                    )
                                    if os.path.exists(filename):
                                        os.remove(filename)
                                    if self.on_discovery[ble_mac] is not None:
                                        self.on_discovery[ble_mac].__ble_not_present__(ble_device)
                                else:
                                    self.logger.info(
                                        "\033[30;42m(DIS)\033[0m \033[1m%s\033[0m %s:%s triggered ON" % (
                                            ble_device.name,
                                            ble_device.mac,
                                            ble_device.rssi)
                                    )
                                    if not os.path.exists(filename):
                                        self._touch(filename, 'risto', 'risto')
                                    if self.on_discovery[ble_mac] is not None:
                                        self.on_discovery[ble_mac].__ble_present__(ble_device)

    def report_location_change(self):
        with lock:
            for mac, ble in list(location.items()):  # type: str, dict
                for scanner_name, ble_info in list(ble.items()):  # type: str, BleDevice
                    gone = ble_info.is_gone()

                    if gone:
                        del location[mac][scanner_name]
                        if len(location[mac]) == 0:
                            del location[mac]

                    if ble_info.is_state_changed(gone):
                        ble_info.set_last_state(gone=gone)
                        if gone:
                            self.logger.info(
                                "\033[30;43m(LFT)\033[0m \033[1m%s\033[0m %s:%s left %s" % (
                                    ble_info.name,
                                    ble_info.mac,
                                    ble_info.rssi,
                                    ble_info.scanner_name
                                )
                            )
                        else:
                            self.logger.info(
                                "\033[30;43m(ARV)\033[0m \033[1m%s\033[0m %s:%s arrived in %s" % (
                                    ble_info.name,
                                    ble_info.mac,
                                    ble_info.rssi,
                                    ble_info.scanner_name)
                            )

                        if self.on_location is not None and scanner_name in self.on_location and mac in \
                                self.on_location[scanner_name]:

                            action = self.on_location[scanner_name][mac]

                            if gone:
                                if action is not None:
                                    action.__ble_not_present__(ble_info)
                            else:
                                if action is not None:
                                    action.__ble_present__(ble_info)
