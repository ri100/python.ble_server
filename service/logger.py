import logging


class LoggerWrapper:

    def __init__(self, file='/var/log/ble_server.log'):
        logging.basicConfig(
            filename=file,
            format='%(asctime)s %(message)s', datefmt='[%m/%d/%Y %I:%M:%S %p]',
            level=logging.DEBUG
        )

    def info(self, text):
        logging.info(text)

    def debug(self, text):
        logging.debug(text)

    def warning(self, text):
        logging.warning(text)

    def error(self, text):
        logging.error(text)

