import requests
from domain.ble_device import BleDevice
from service.logger import LoggerWrapper


class WebAlarm:

    def __init__(self, url: str):
        self.url = url  # http://192.168.1.123:1880/monitoring
        self.logger = LoggerWrapper()

    def _request(self, state, time_span, ble_device: BleDevice):
        body = {
            "state": state,
            "mac": ble_device.mac,
            "gone_for": time_span,
            "name": ble_device.name,
            "rssi": ble_device.rssi,
            "timestamp": ble_device.time
        }
        try:
            requests.post(url=self.url, json=body)
        except Exception as e:
            self.logger.error('\033[31;1m[ERR]\033[0m %s' % str(e))

    def _notify(self, ble_device: BleDevice, state):
        self.logger.info("%s \033[1m%s\033[0m %s %s last seen at %s %f sec ago" % (
            state.upper(), ble_device.mac, ble_device.name, ble_device.mac, ble_device.last_seen_date(),
            ble_device.last_seen_sec()))
        self._request(state, ble_device.gone_for(), ble_device)

    def __ble_present__(self, ble_device: BleDevice):
        return self._notify(ble_device, 'in')

    def __ble_not_present__(self, ble_device: BleDevice):
        return self._notify(ble_device, 'out')
