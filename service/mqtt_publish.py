import paho.mqtt.client as mqtt
from domain.ble_device import BleDevice
from service.logger import LoggerWrapper


class MqttPublish:

    def __init__(self, topic: str, body: dict, address: str, credentials: tuple, port: int = 1883):
        self.user, self.password = credentials
        self.topic = topic  # 'cmnd/tasmota1/power'
        self.body = body
        self.port = port
        self.address = address  # "192.168.1.123"
        self.logger = LoggerWrapper()

        self.mqttc = mqtt.Client()
        self.mqttc.username_pw_set(self.user, password=self.password)
        self.mqttc.connect(self.address, self.port, 60)

    def __ble_present__(self, ble_device: BleDevice):
        self.mqttc.publish(self.topic, self.body['ON'], qos=1)
        self.logger.info("[BRK] %s %s" % (self.topic, self.body['ON']))

    def __ble_not_present__(self, ble_device: BleDevice):
        self.mqttc.publish(self.topic, self.body['OFF'], qos=1)
        self.logger.info("[BRK] %s %s" % (self.topic, self.body['OFF']))
