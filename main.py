from domain.interval import Interval
from domain.location_report import LocationReport
from service.logger import LoggerWrapper
from service.notification_manager import NotificationManager
from service.server import BleServer
from service.web_alarm import WebAlarm
from domain.one_of import OneOf

# Grouping allows to identify a set of BLE devices as one.
# This means that when one of the device is detected then notification is triggered

accepted_devices_groups = {
    'Owner': OneOf(['cb:5a:bb:cc:cb:33', 'e2:da:83:48:32:e1']),  # Keys
    'Blinders': OneOf(['cb:5a:bb:cc:cb:33', 'e2:da:83:48:32:e1']),
    'Magda': OneOf(['e2:da:83:48:32:e1']),
    'Risto': OneOf(['cb:5a:bb:cc:cb:33'])
}

timeouts = {
    'Owner': 90,
    'Blinders': 600,
    'Magda': 90,
    'Risto': 120
}

address = ('', 1234)
thread = BleServer(
    server_timeout=20,
    bind=address,
    devices_timeouts=timeouts,
    grouping=accepted_devices_groups)
thread.start()

logger = LoggerWrapper()

try:
    ble_alarm = NotificationManager(
        on_discovery_change={
            # Keys
            'Owner': WebAlarm(
                url='http://192.168.1.123:1880/ble/owner'  # call when device discovered
            ),
            'Risto': WebAlarm(
                url='http://192.168.1.123:1880/ble/risto'  # call when device discovered
            ),
            'Magda': WebAlarm(
                url='http://192.168.1.123:1880/ble/magda'  # call when device discovered
            ),
        },
        # on_location_change={
        #     'Kitchen': {'JBL': WebAlarm(
        #         url='http://192.168.1.123:1880/ble'  # call when device discovered
        #     )}
        # },
        # report_location=[
        #     # reports location of given ble groups in interval of 30 sec
        #     LocationReport(of=['Risto'], every=Interval(120), send_to="http://192.168.1.123:1880/ble/location"),
        # ]
    )
    ble_alarm.notify()

except (KeyboardInterrupt, SystemExit):
    logger.info("[INF] Waiting for server shut down")
    thread.shutdown_flag.set()
    thread.join(timeout=30)

