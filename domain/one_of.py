class OneOf:

    def __init__(self, macs: list):
        self.macs = macs

    def has_mac(self, mac):
        return mac in self.macs
