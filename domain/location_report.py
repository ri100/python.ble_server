from domain.ble_device import BleDevice
from domain.interval import Interval


class LocationReport(object):

    def __init__(self, of, every: Interval, send_to):
        self.send_to = send_to
        self.every = every
        self.of = of
        self.report = {}

    def prepare_data(self, locations):
        self.report = {}

        """
               Prepares data in form of
               {
                   "group1": {loc1": -92, "loc2": -67},
                   "group1": {loc1": -92, "loc2": -67}
               }
        """

        for mac, ble in locations.items():  # type: str, dict
            for scanner_name, ble_info in ble.items():  # type: str, BleDevice
                if mac in self.of:
                    if mac in self.report:
                        self.report[mac].update({scanner_name: int(ble_info.rssi)})
                    else:
                        self.report[mac] = {scanner_name: int(ble_info.rssi)}

        return self.report


if __name__ == '__main__':
    loc = LocationReport(of=['group1', 'group3'], every=60)

    locations = {"Kitchen": {"group1": BleDevice('name', '00:00:00:00:00:00', 30),
                             "group2": BleDevice('name', '00:00:00:00:00:00', 30),
                             "group3": BleDevice('name', '00:00:00:00:00:00', 30)},
                 "Garage": {"group1": BleDevice('name', '00:00:00:00:00:00', 30)}
                 }
    report = loc.prepare_data(locations)

    print(report)
