from time import time


class Interval:

    def __init__(self, interval):
        self.interval = interval
        self.timestamp = int(time())

    def passed(self):
        if time() > self.timestamp+self.interval:
            self.timestamp = int(time())
            return True
        return False
