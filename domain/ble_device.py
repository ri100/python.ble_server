from time import time
from datetime import datetime


class BleDevice:
    def __init__(self, name, mac, expire_in=30, rssi: int=-100):
        self.scanner_name = "unknown"
        self.name = name
        self.time = None
        self.mac = mac
        self.expire_in = float(expire_in)
        self.gone = None
        self.rssi = rssi

    def update(self, timestamp: int = None, rssi: int = None, scanner_name: str = "unknown", ) -> int:
        """
        Returns 0 if OK, less then 0 if not ok

        :param scanner_name:
        :param timestamp:
        :param rssi:
        :return:
        """

        self.scanner_name = scanner_name

        now = time()

        if timestamp is None:
            self.time = now
        else:
            timestamp = float(timestamp)

            # problem of year 2038
            if timestamp >= 2147483647:
                timestamp = 2147483647

            # is this first update
            if self.time is None:
                self.time = timestamp
                return True

            # Check if not with higher timestamp
            if int(self.time) > int(timestamp):
                return False

            # Future can no be more then 60s from now
            if timestamp > now + 60:
                return False

            self.time = timestamp

        if rssi is not None:
            self.rssi = rssi

        return True

    def gone_for(self):
        return time() - self.time

    def is_gone(self):
        return self.time + self.expire_in < time()

    def is_mac(self, mac):
        return mac == self.mac

    def last_seen_date(self):
        return datetime.fromtimestamp(self.time).strftime('%Y-%m-%d %H:%M:%S')

    def last_seen_sec(self):
        return time() - self.time

    def set_last_state(self, gone):
        self.gone = gone

    def is_state_changed(self, gone):
        return gone != self.gone
